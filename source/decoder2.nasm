[bits 64]
global decode_run_clean
decode_run_clean:
	; char *d = rdi
	; char *e = rsi
	; size_t/char n = rdx/dl
	; size_t/char m = rcx/cl

	movzx rdx, dl
	movzx rcx, cl
	mov r9, rdx		; n_saved = r9
	xchg rdx, rcx	; n, m = rcx, rdx
	for0:
		xor rax, rax
		xor rbx, rbx

		sub rcx, r9
		neg rcx

		for1:
			mul cl
			mov r10, rax
			xor ax, ax
			lodsb
			add r10w, ax
			mod:
				cmp r10, rdx
				jl mod_done
				sub r10, rdx
				jmp mod
			mod_done:
			mov rax, r10
			inc rbx
			cmp rbx, r9
			jl for1
		;sub rsi, r9
		;add rsi, r9
		mov al, [rsi+rax]
		sub rsi, r9
		stosb

		neg rcx
		add rcx, r9

		loop for0

	mov rax, 0xc390909090909090
	stosq

	sub rdi, 8
	sub rdi, r9
	
	push rsi
	push r9
	push rdx

	call rdi

	pop rdx
	pop r9
	pop rsi

	lea rax, [rsi+r9]
	add rax, rdx
	jmp rax
