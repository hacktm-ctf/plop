from pwn import *
import random
from enc import encode, decode

context.arch = 'amd64'

def rol64(n, i):
	i = i % 64
	return ((n << i) | (n >> (64-i))) & 0xffffffffffffffff

flag = r"HackTM{PolynomialLookupOrientedProgramming_sounds_kinda_shit_xd}"
assert(len(flag) == 64)
chunks = [u64(flag[i:i+8]) for i in range(0, 64, 8)]
enc = [None]*8
target = os.urandom(8)

for i in range(8):
	s = ord(os.urandom(1))
	c = p64(rol64(chunks[i], s))
	enc[i] = (s, xor(target, c))

base_flag_address = 0x1337000

asm_variants = [
# [0] WORKS
"""
mov rax, [{addr}]
rol rax, {shift}

mov rdx, {xork}
xor rax, rdx
/* mov [0x1337088], rax */

mov r8, [0x1337080]
cmp r8, 0
jne not_first
mov [0x1337080], rax
jmp done
not_first:
cmp rax, [0x1337080]
mov bx, [0x1337064]
mov ax, 1
cmovne bx, ax
mov [0x1337064], bx
done:
""",


# [1] WORKS
"""
mov rbx, {chunk_index}
lea rax, [0x1337000 + 8*rbx]
mov rax, [rax]

mov r8, [0x1337080]

mov rdx, {xork}
rol rdx, {shift_inv}
xor rax, rdx

cmp r8, 0
je first
ror rax, {shift_inv}
cmp rax, [0x1337080]
je done
mov byte ptr [0x1337064], 1
jmp done
first:
rol rax, {shift}
mov [0x1337080], rax
done:
/* mov [0x1337088], rax */
""",

# [2] BROKEN
"""
mov rbx, [0x1337000+8*{chunk_index}]

mov r9, [0x1337080]
mov rax, [0x1337080]
mov rdx, {xork}
xor r9, rdx
ror r9, {shift}
test rax, rax
jz first
cmp rbx, r9
je done
mov byte ptr [0x1337064], 1
jmp done
first:
rol rbx, {shift}
xor rbx, rdx
mov [0x1337080], rbx
/* mov r9, rbx */
done:
/* ror r9, {shift_inv}
mov [0x1337088], r9 */
"""
]

# text ROL XOR CMP enc
# enc
#    first: enc = text ROL XOR CMP
#    after: enc XOR ROR CMP text

obf = ""


order = [0,1,2,3,4,5,6,7]
v = []
#random.shuffle(order)
for i in order:
	rand_variant = random.choice(asm_variants)
	v.append(asm_variants.index(rand_variant))
	code = asm(rand_variant.format(xork=u64(enc[i][1]), xorkLOW=u64(enc[i][1])%0x100000000, xorkHI=u64(enc[i][1])/0x100000000, chunk_index=i, addr=0x1337000+8*i, shift=enc[i][0]%64, shift_inv=64-(enc[i][0]%64)))
	#print code.encode("hex")
	code = [ord(q) for q in code]
	code_n, code_enc = encode(code)
	
	code_m = len(code_enc) - code_n

	obf += asm("mov rax, [{}]".format(code_n * 256 + code_m))
	obf += ''.join(chr(q) for q in code_enc)

#obf += asm("ret")

#print obf.encode("hex")

with open("checker.nasm", "w") as f:
	f.write("""
[bits 64]
global badshit
badshit:
""")
	f.write("db ");
	f.write(",".join(hex(ord(q)) for q in obf))
	f.write("""
mov rax, [0x1337100]
push rax
ret
""")
"""
print "Target xor: ", ''.join(target[::-1]).encode("hex")
print "Variants chosen: ", v
print "Order          : ", order

for i in order:
	print i, "-----"
	print "{} ROL {} = {} == {} ^ target".format(hex(chunks[i]), enc[i][0], p64(rol64(chunks[i], enc[i][0])).encode("hex"), enc[i][1][::-1].encode("hex"))
"""
os.system("nasm -felf64 checker.nasm")
