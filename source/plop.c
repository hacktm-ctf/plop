#define __USE_GNU
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/ptrace.h>
#include <sys/ucontext.h>

#define SECRET_MEM (void*)0x1337000

char flag[101];
bool flag_bad = false;

void after_3_seconds(int sig) {
	printf("\nThe results are in: %s\n", !flag_bad ? "You did it!" : "Meh try again :/");
	exit(flag_bad);
}

extern void print_ellipsis_obf(int*);

static void print_ellipsis() {
	static int dots_printed = 0;

	if(dots_printed < 5) {
		dots_printed++;
		putchar('.');
		fflush(stdout);
		alarm(1);
	} else {
		signal(SIGALRM, after_3_seconds);
		alarm(1);
	}
}

int main() {
	puts(
" /--------------------------\\ \n"
" |                          | \n"
" | Welcome to my challenge! | \n"
" |                          | \n"
" \\--------------------------/ \n"
	);

	printf("Lemme check your flag :");
	scanf("%100s", flag);
	puts("Thank you! We'll get back to you later with the results.");

	signal(SIGALRM, &print_ellipsis);
	alarm(1);

	return 0;
}

extern void badshit(void);

void checks_done(void) {
	char *p = 0x1337064;
	flag_bad = *p;

	while(1);
}

// FINI list (executed from the bottom up)
__attribute__((destructor)) static void fini2(void) {
	printf("\nThe lab is checking your flag (%s)", flag);

	badshit();

	__builtin_unreachable();
}

extern void decode_run_clean(char *d, char *e, char n, char m);
/*void decode_run_clean(char *d, char *e, char n, char m) {
	char *lut = &e[n];

	for(int i=0; i<n; i++) {
		int q=0;
		for(int j=0; j<n; j++) {
			q = (q * i + e[j]) % m;
		}
		d[i] = lut[q];
	}

	for(int i=0; i<100; i++)
		d[i+n] = 0x90;

	d[n+100] = 0xc3;

	typedef void (*func_ptr)();
	((func_ptr)d)();

	for(int i=100+n; i>0; i-=8) memcpy(d+i, "69420lol", 8);

	void *ptr = (void *)e+m+n;
	goto *ptr;
}*/

static int ptrace_checked = 0;

void handle_sigsegv(int sig, siginfo_t *si, void *uc) {
	// Fake thingy to fool ltrace/strace; won't fool gdb tho :/
	/*if(!ptrace_checked++ && ptrace(PTRACE_TRACEME, 0, 1, 0) == -1) {
		write(1, "Segmentation fault (core dumped)\n", 33);
		exit(139);
	}*/
	long long **abs = 0x1337100;
	*abs = &checks_done;

	ucontext_t *u = (ucontext_t *)uc;
	#define REG_RIP 16
	unsigned char *pc = (unsigned char *)u->uc_mcontext.gregs[REG_RIP];
	#undef REG_RIP

	/*printf("\n-----\n");
	printf("at: %x\n", pc);
	printf("0x1337064: %d\n", *((int*)0x1337064));
	printf("0x1337080: %llx\n", *((long long*)0x1337080));
	printf("0x1337088: %llx\n", *((long long*)0x1337088));*/

	char n = pc[5], m = pc[4];
	char *d = mmap(NULL, 0x1000, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
	decode_run_clean(d, &pc[8], n, m);
	munmap(d, 0x1000);
}

__attribute__((destructor)) static void fini1(void) {
	if(SECRET_MEM != mmap(SECRET_MEM, 0x1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, 0, 0)) {
		exit(0);	// can't allocate
	}

	memcpy(SECRET_MEM, flag, 100);

	struct sigaction sa;

	sa.sa_sigaction = &handle_sigsegv;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_SIGINFO | SA_NODEFER;	// nodefer because the handler will cause itself to run again

	sigaction(SIGSEGV, &sa, 0);
}
