import random
import sys
import os

def encode(d):
	d = list(d)
	n = len(d)
	assert(n < 255)	# could do <256 but that makes the assembly code slightly more fiddly

	primes = sorted([2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251])
	unique_bytes = set(d)
	assert(len(unique_bytes) <= primes[-1])

	min_m = n #len(unique_bytes)
	m = random.choice(primes)
	while m < min_m:
		m = random.choice(primes)

	modinv = [None] * m
	for i in range(1, m):
		for j in range(1, m):
			if i*j % m == 1:
				modinv[i] = j
				break
		else:
			raise ValueError("No inverse for {} modulo {}".format(i, m))

	LUT = list(unique_bytes)
	random.shuffle(LUT)
	LUT = LUT + [ord(q) for q in os.urandom(m - len(LUT))]

	d_lut = []
	for c in d:
		d_lut.append(LUT.index(c))

	e = [0] * n
	for i in range(n):
		num = [0] * n
		num[-1] = 1
		denom = 1

		for j in range(n):
			if i == j: continue

			denom = denom * (i-j) % m
			
			# Multiply num[] by (x-i)
			# num[k] = num[k+1] - i * num[k]
			for k in range(n):
				if k < n-1:
					num[k] = num[k+1] - j * num[k]
				else:
					num[k] = -j * num[k]
				num[k] = num[k] % m

		denom_inv = modinv[denom]
		for k in range(n):
			e[k] = (e[k] + d_lut[i] * num[k] * denom_inv) % m

	for i in range(n):
		e[i] = e[i] % m

	return n, e + LUT


def format(n, obf = None):
	if obf == None:
		n, obf = n[0], n[1]

	return "; n = {} -> mov dl, {}\ndb {}\n".format(n, n, ','.join(hex(a) for a in obf))

#print format(encode([1, 2, 3, 4]))

def decode(n, obf = None):
	if obf == None:
		n, obf = n[0], n[1]

	m = len(obf) - n
	lut = obf[n:]

	d = [None] * n
	for i in range(n):
		q = 0
		for j in range(n):
			q = (q * i + obf[j]) % m
		d[i] = lut[q]

	return d

#print decode(encode([1, 2, 99, 123, 47]))


"""

... 3, 4, 10    = 10 + 4x + 3x^2
* x-10
= 10x + 4x^2 + 3x^3 -100 -40x -30x^2
= -100 + (10-40)x + (4-30)x^2 + 3x^3
... 3, (4-30), (10-40), -100

"""